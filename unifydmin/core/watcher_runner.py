import threading
import time
from .singleton import Singleton
from gi.repository import GObject

class WatcherRunnerSignaler(GObject.Object):
    __gsignals__ = {
        'unifydmin_watcher_new_data': (
            GObject.SIGNAL_RUN_LAST,
            None,
            (str,)
        )
    }

class WatcherRunner(metaclass=Singleton):
    def __init__(self):
        self._thread = threading.Thread(
            name = 'WatcherRunner thread',
            group = None,
            target = self._async_worker
        )
        self.systems = []
        self._thread_stop = True
        self.signaler = WatcherRunnerSignaler()
        self.connect = self.signaler.connect
        self.emit = self.signaler.emit

    def add_system(self, system):
        self.systems.append(system)

    def del_system_by_name(self, name):
        for index, system in enumerate(self.systems):
            if system.name == name:
                self.systems.pop(index)
                break

    @property
    def thread_alive(self):
        return self._thread.is_alive()

    def start_thread(self):
        self._thread_stop = False
        self._thread.start()

    def stop_thread(self):
        self._thread_stop = True

    def _async_worker(self):
        while not self._thread_stop:
            start_time = time.time()
            for s in self.systems:
                for w in s.watchers:
                    try:
                        w._worker()
                    except:
                        print(f'Error running watcher `{w}` on system `{s}`')
            self.emit('unifydmin_watcher_new_data' , '')
            time_to_wait = 2 - (time.time() - start_time)
            if time_to_wait > 0:
                time.sleep(time_to_wait)
