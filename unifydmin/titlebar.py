from gi.repository import Gtk, Handy

class UnifydminLeafletTitlebar(Handy.TitleBar):
    def __init__(self, left_headerbar, right_headerbar_stack, **kwargs):
        super().__init__(**kwargs)

        # self.separator = Gtk.VSeparator()

        # Add all(?) headerbars to it with add_header_bar
        self.headergroup = Handy.HeaderGroup()

        self.leaflet = Handy.Leaflet()
        # animation disabled, it causes this on each fold
        # Gtk-CRITICAL **: 12:44:19.099: gtk_widget_draw: assertion '!widget->priv->alloc_needed' failed
        #self.leaflet.set_mode_transition_type(Handy.LeafletModeTransitionType.SLIDE)
        
        self.left_headerbar = left_headerbar
        self.right_headerbar_stack = right_headerbar_stack

        # initially have the two current headerbar pieces in the headergroup
        # it changes in appWindow.UnifydminAppWindow.on_main_leaflet_folded
        self.headergroup.add_header_bar(self.left_headerbar.headerbar)
        self.headergroup.add_header_bar(
            self.right_headerbar_stack.get_children()[0].child_widget.headerbar
        )

        self.leaflet.add(self.left_headerbar)
        self.leaflet.add(self.right_headerbar_stack)

        self.add(self.leaflet)

    def leaflet_switch_visible(self):
        if self.leaflet.get_visible_child() == self.left_headerbar:
            self.leaflet.set_visible_child(self.right_headerbar_stack)
        else:
            self.leaflet.set_visible_child(self.left_headerbar)

    def leaflet_set_visible_by_name(self, name):
        if name == 'sidebar':
            self.leaflet.set_visible_child(self.left_headerbar)
        elif name == 'stack':
            self.leaflet.set_visible_child(self.right_headerbar_stack)
        else:
            raise AttributeError('You need to pass either \'sidebar\' or \'stack\'')
