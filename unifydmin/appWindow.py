from gi.repository import Gtk
from .titlebar import UnifydminLeafletTitlebar
from .leaflet import UnifydminLeaflet
from .appStack import UnifydminAppStack
from .systemView import UnifydminSystemView
from .actionView import UnifydminActionView
from .confManager import ConfManager


class UnifydminAppWindow(Gtk.ApplicationWindow):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        # this grabs a pre-existing instance, ConfManager is singleton
        self.confman = ConfManager()

        self.box = Gtk.Box(Gtk.Orientation.VERTICAL, 0)
        self.box.set_orientation(Gtk.Orientation.VERTICAL)
        self.box.set_homogeneous(False)

        self.size_group_left = Gtk.SizeGroup(Gtk.SizeGroupMode.HORIZONTAL)
        self.size_group_right = Gtk.SizeGroup(Gtk.SizeGroupMode.HORIZONTAL)

        self.systems = self.confman.systems
        self.actions = self.confman.actions

        self.main_leaflet = UnifydminLeaflet()
        self.sidebar = self.main_leaflet.sidebar
        self.app_stack = self.main_leaflet.main_stack
        self.right_headerbar_stack = UnifydminAppStack()
        self.sidebar.set_stacks(self.app_stack, self.right_headerbar_stack)

        self.sidebar.populate_systems_list(self.systems)
        self.sidebar.populate_actions_list(self.actions)
        self.sidebar.headerbar.new_system_widget.connect_button.connect('clicked', self.add_system)

        for s in self.systems:
            view = UnifydminSystemView(s)
            self.app_stack.add_and_get_stackchild(
                s.name,
                view
            )
            #view.headerbar.headerbar.set_show_close_button(True)
            self.right_headerbar_stack.add_and_get_stackchild(
                s.name,
                view.headerbar
            )
        
        for a in self.actions:
            view = UnifydminActionView(a)
            view.connect(
                'unifydmin_action_running_changed',
                self.on_actionView_action_running_changed
            )
            self.app_stack.add_and_get_stackchild(
                a.name,
                view
            )
            #view.headerbar.headerbar.set_show_close_button(True)
            self.right_headerbar_stack.add_and_get_stackchild(
                a.name,
                view.headerbar
            )

        self.left_headerbar = self.sidebar.headerbar

        # needs left headerbar and right headerbar stack
        self.titlebar = UnifydminLeafletTitlebar(
            self.left_headerbar,
            self.right_headerbar_stack
        )

        for c in self.right_headerbar_stack.get_children():
            # bb back_button, connecting its signal to show the sidebar
            bb = c.child_widget.back_button
            bb.connect('clicked', self.leaflet_show_sidebar)

        self.size_group_left.add_widget(self.sidebar)
        self.size_group_left.add_widget(self.left_headerbar)

        self.size_group_right.add_widget(self.app_stack)
        self.size_group_right.add_widget(self.right_headerbar_stack)

        self.set_titlebar(self.titlebar)
        self.box.pack_start(self.main_leaflet, True, True, 0)

        self.add(self.box)

        self.main_leaflet.connect('notify::folded', self.on_main_leaflet_folded)
        # call on_main_leaflet_folded manually to have a correct starting state
        self.on_main_leaflet_folded()
        # connect the headerbar change to new sidebar selection, so that the correct
        # right headerbar is always used
        self.sidebar.connect('unifydmin_stack_changed', self.on_main_leaflet_folded)
        self.confman.connect('unifydmin_system_deleted', self.del_system)

        # accel_group is for keyboard shortcuts
        self.accel_group = Gtk.AccelGroup()
        self.add_accel_group(self.accel_group)
        shortcuts_l = [
            {
                'combo': '<Shift>j',
                'cb': self.sidebar.select_next_row
            },
            {
                'combo': '<Shift>k',
                'cb': self.sidebar.select_prev_row
            },
            {
                'combo': '<Shift>h',
                'cb': self.leaflet_show_sidebar
            },
            {
                'combo': '<Shift>l',
                'cb': self.leaflet_show_appstack
            },
            {
                'combo': '<Shift>Return',
                'cb': self.on_shift_enter
            },
            {
                'combo': '<Control>q',
                'cb': self.destroy
            }
        ]
        for s in shortcuts_l:
            self.add_accelerator(s['combo'], s['cb'])
        
    def add_accelerator(self, shortcut, callback):
        if shortcut:
            key, mod = Gtk.accelerator_parse(shortcut)
            self.accel_group.connect(key, mod, Gtk.AccelFlags.VISIBLE, callback)

    def on_shift_enter(self, *args):
        self.app_stack.get_visible_child().child_widget.on_shift_enter()

    def destroy(self, *args):
        self.emit('destroy')

    def on_actionView_action_running_changed(self, view, state):
        for child in self.app_stack.get_children():
            if type(child.child_widget) == UnifydminActionView and child.child_widget != view:
                child.child_widget.headerbar.run_btn.set_sensitive(not state)
                child.child_widget.headerbar.run_btn.set_label('Busy' if state else 'Run')

    def add_system(self, btn):
        name = self.sidebar.headerbar.new_system_widget.entry_name.get_text()
        address = self.sidebar.headerbar.new_system_widget.entry_address.get_text()
        username = self.sidebar.headerbar.new_system_widget.entry_username.get_text()
        n_system = self.confman.add_system({'name': name, 'address': address, 'username': username})
        self.sidebar.populate_systems_list(self.systems)
        view = UnifydminSystemView(n_system)
        self.app_stack.add_and_get_stackchild(
            n_system.name,
            view
        )
        self.right_headerbar_stack.add_and_get_stackchild(
            n_system.name,
            view.headerbar
        )
        view.headerbar.back_button.connect('clicked', self.leaflet_show_sidebar)
        self.app_stack.show_all()
        self.right_headerbar_stack.show_all()
        self.sidebar.headerbar.new_system_widget.clear_fields()
        self.sidebar.headerbar.new_system_popover.popdown()
        for child in self.app_stack:
            if type(child.child_widget) == UnifydminActionView:
                child.child_widget.populate_systems_list()

    def del_system(self, signaler, name):
        for stack in [self.app_stack, self.right_headerbar_stack]:
            stack.remove(stack.get_child_by_name(name))
        for child in self.app_stack:
            if type(child.child_widget) == UnifydminActionView:
                child.child_widget.populate_systems_list()
        self.sidebar.populate_systems_list(self.systems)
        self.main_leaflet.set_visible_by_name('sidebar')
        self.titlebar.leaflet_set_visible_by_name('sidebar')

    def set_show_back_buttons(self, state):
        for c in self.app_stack.get_children():
            c = c.child_widget
            if state:
                c.back_button.show()
            else:
                c.back_button.hide()

    def leaflet_show_sidebar(self, *args):
        self.main_leaflet.set_visible_child(self.sidebar)
        self.titlebar.leaflet.set_visible_child(self.sidebar.headerbar)
        self.on_main_leaflet_folded()

    def leaflet_show_appstack(self, *args):
        self.main_leaflet.set_visible_child(self.app_stack)
        self.titlebar.leaflet.set_visible_child(self.right_headerbar_stack)
        self.on_main_leaflet_folded()

    def on_main_leaflet_folded(self, *args):
        if len(args) >= 1:
            if args[0] == self.sidebar:
                # code here gets ran only when sidebar list is activated
                self.main_leaflet.set_visible_by_name('stack')
                self.titlebar.leaflet_set_visible_by_name('stack')
        leaflet = self.main_leaflet
        left_hbar = self.left_headerbar.headerbar
        hgroup = self.titlebar.headergroup

        if leaflet.folded:
            self.set_show_back_buttons(True)
        else:
            self.set_show_back_buttons(False)

        if leaflet.folded and leaflet.get_visible_child() == self.sidebar:
            hgroup.set_focus(left_hbar)
        else:
            for hbar in hgroup.get_header_bars():
                if hbar != left_hbar:
                    hgroup.remove_header_bar(hbar)
                    break
            r_hbar_stack_visible_child = self.right_headerbar_stack.get_visible_child()
            n_right_hbar = None
            if r_hbar_stack_visible_child:
                n_right_hbar = r_hbar_stack_visible_child.child_widget.headerbar
            else:
                n_right_hbar = self.right_headerbar_stack.get_children()[0].child_widget.headerbar
            hgroup.add_header_bar(n_right_hbar)
            hgroup.set_focus(n_right_hbar)
