from gi.repository import Gtk

class UnifydminShortcutsWindow(Gtk.ShortcutsWindow):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.builder = Gtk.Builder.new_from_string('''
<interface>
  <object class="GtkShortcutsWindow" id="shortcuts-unifydmin">
    <property name="modal">1</property>
    <child>
      <object class="GtkShortcutsSection">
        <property name="section-name">shortcuts</property>
        <property name="max-height">12</property>
        <child>
          <object class="GtkShortcutsGroup">
            <property name="title" translatable="yes">General</property>
            <child>
              <object class="GtkShortcutsShortcut">
                <property name="accelerator">&lt;ctrl&gt;Q</property>
                <property name="title" translatable="yes">Quit</property>
              </object>
            </child>
            <child>
              <object class="GtkShortcutsShortcut">
                <property name="accelerator">&lt;shift&gt;J</property>
                <property name="title" translatable="yes">Next system/action</property>
              </object>
            </child>
            <child>
              <object class="GtkShortcutsShortcut">
                <property name="accelerator">&lt;shift&gt;K</property>
                <property name="title" translatable="yes">Previous system/action</property>
              </object>
            </child>
            <child>
              <object class="GtkShortcutsShortcut">
                <property name="accelerator">&lt;shift&gt;Enter</property>
                <property name="title" translatable="yes">Open SSH session/Run action</property>
              </object>
            </child>
          </object>
        </child>
      </object>
    </child>
  </object>
</interface>
        ''')
