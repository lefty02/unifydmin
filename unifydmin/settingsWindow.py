from gi.repository import Gtk, Gdk
from .confManager import ConfManager

class UnifydminSettingsWindow(Gtk.Window):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.confman = ConfManager()

        self.set_skip_taskbar_hint(True)
        self.set_skip_pager_hint(True)
        self.set_type_hint(Gdk.WindowTypeHint.DIALOG)

        self.builder = Gtk.Builder.new_from_resource(
            '/org/gabmus/unifydmin/ui/settingsWindow.glade'
        )

        self.set_titlebar(self.builder.get_object('settingsHeaderbar'))
        self.add(self.builder.get_object('settingsBox'))

        self.terminal_cmd_entry = self.builder.get_object('terminalCmdEntry')
        self.terminal_cmd_entry.set_text(self.confman.conf['terminal_cmd'])

        self.builder.connect_signals(self)

    def on_terminalCmdSaveButton_clicked(self, btn):
        self.confman.conf['terminal_cmd'] = self.terminal_cmd_entry.get_text()
        self.confman.save_conf()

    def on_terminalCmdResetDefault_clicked(self, btn):
        self.terminal_cmd_entry.set_text(self.confman.BASE_SCHEMA['terminal_cmd'])
