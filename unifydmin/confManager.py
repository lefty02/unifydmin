# from .core.system import System
from gi.repository import GObject
from .core.singleton import Singleton
from pathlib import Path
from os.path import isfile, isdir
from os import makedirs
from os import environ as Env
import json
from .core.basic_actions import (
    UpdateSystem,
    RebootSystem,
    DockerProcesses,
    SystemdServices,
    OpenPorts,
    CpuUsage,
    MemoryUsage
)
from .core.system import System
from .core.watcher_runner import WatcherRunner

class ConfManagerSignaler(GObject.Object):
    __gsignals__ = {
        'unifydmin_system_deleted': (
            GObject.SIGNAL_RUN_LAST,
            None,
            (str,)
        )
    }

class ConfManager(metaclass=Singleton):

    BASE_SCHEMA = {
        'systems': [
            
        ],
        'terminal_cmd': 'xterm -fa "Monospace" -fs 14 -bg "#1d1f21" -fg "#c5c8c6" -e'
    }

    def __init__(self):

        # check if inside flatpak sandbox
        self.is_flatpak = False
        if 'XDG_RUNTIME_DIR' in Env.keys():
            if isfile(f'{Env["XDG_RUNTIME_DIR"]}/flatpak-info'):
                self.is_flatpak = True

        if self.is_flatpak:
            self.path = Path(f'{Env.get("XDG_CONFIG_HOME")}/org.gabmus.unifydmin.json')
        else:
            self.path = Path(f'{Env.get("HOME")}/.config/org.gabmus.unifydmin.json')

        self.stop_threads = False

        if not isdir(self.path.parent):
            makedirs(self.path.parent)

        self.conf = None
        if isfile(self.path):
            with open(self.path) as fd:
                self.conf = json.loads(fd.read())
                fd.close()
            # verify that the file has all of the schema keys
            for k in self.BASE_SCHEMA.keys():
                if not k in self.conf.keys():
                    if type(self.BASE_SCHEMA[k]) in [list, dict]:
                        self.conf[k] = self.BASE_SCHEMA[k].copy()
                    else:
                        self.conf[k] = self.BASE_SCHEMA[k]
        else:
            self.conf = self.BASE_SCHEMA.copy()
        
        self.signaler = ConfManagerSignaler()

        self.actions = [UpdateSystem(), RebootSystem()]
        self.watcher_actions = [CpuUsage, MemoryUsage, OpenPorts, SystemdServices, DockerProcesses]
        #self.watcher_actions = [CpuUsage]
        self.watcher_runner = WatcherRunner()
        self.systems = []
        for s in self.conf['systems']:
            self.add_system(s)

        self.watcher_runner.start_thread()

    def connect(self, *args, **kwargs):
        self.signaler.connect(*args, **kwargs)

    def emit(self, *args, **kwargs):
        self.signaler.emit(*args, **kwargs)

    def save_conf(self):
        self.conf['systems'] = []
        for s in self.systems:
            self.conf['systems'].append(s.to_dict())
        with open(self.path, 'w') as fd:
            fd.write(json.dumps(self.conf))
            fd.close()

    def exists_system_with_name(self, name):
        for s in self.systems:
            if s.name == name:
                return True
        return False

    def add_system(self, n_system):
        assert type(n_system) == dict
        assert 'name' in n_system.keys()
        assert 'address' in n_system.keys()
        assert 'username' in n_system.keys()
        assert not self.exists_system_with_name(n_system['name'])

        system_obj = System(n_system['name'], n_system['address'], n_system['username'])
        for w in self.watcher_actions:
            system_obj.add_watcher(w)
        self.systems.append(system_obj)
        self.watcher_runner.add_system(system_obj)
        self.save_conf()
        return system_obj

    def del_system_by_name(self, name):
        for index, system in enumerate(self.systems):
            if system.name == name:
                self.systems.pop(index)
                break
        self.watcher_runner.del_system_by_name(name)
        self.save_conf()
        self.emit('unifydmin_system_deleted', name)


