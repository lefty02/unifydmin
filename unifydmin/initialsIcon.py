from gi.repository import Gtk, Pango

class UnifydminInitialsIcon(Gtk.Bin):
    def __init__(self, name, **kwargs):
        super().__init__(**kwargs)
        self.name = name
        namesplit = self.name.split()
        if len(namesplit) >= 2:
            self.initials = f'{namesplit[0][0]}{namesplit[1][0]}'.upper()
        else:
            self.initials = f'{self.name[0]}{self.name[1]}'.upper()
        self.icon_overlay = Gtk.Overlay()
        image = Gtk.Image.new_from_icon_name('media-record-symbolic', Gtk.IconSize.INVALID)
        image.set_pixel_size(60)
        label = Gtk.Label()
        label.set_markup(f'<span fgcolor="#BBBBBB" weight="heavy">{self.initials}</span>')
        self.icon_overlay.add(image)
        self.icon_overlay.add_overlay(label)
        self.icon_overlay.set_margin_left(6)
        self.icon_overlay.set_margin_right(6)

        self.add(self.icon_overlay)
