# Acknowledgement: I relied on https://gitlab.com/leinardi/gwe/blob/release/gwe/view/historical_data.py for the dazzle powered graphs. Thanks to Roberto Leinardi (https://gitlab.com/leinardi) for the huge (if indirect) help with this part.

from gi.repository import Gtk, Gdk, Dazzle, GLib, GObject
from .graph_stacked_renderer import GraphStackedRenderer
from gi.repository.GObject import TYPE_DOUBLE # for the type of data the graph wants
from .confManager import ConfManager
from .core.watcher_runner import WatcherRunner

class UnifydminGraphBox(Gtk.Bin):
    def __init__(self, watcher, **kwargs):
        super().__init__(**kwargs)
        self.watcher = watcher
        self.watcher_runner = WatcherRunner()
        self.watcher_runner.connect('unifydmin_watcher_new_data', self.on_watcher_new_data)
        self.confman = ConfManager()

        self.container_box = Gtk.Box(Gtk.Orientation.VERTICAL)
        self.container_box.set_orientation(Gtk.Orientation.VERTICAL)
        self.container_box.set_homogeneous(False)
        self.label = Gtk.Label(self.watcher.name)
        self.label.set_margin_left(6)
        self.label.set_halign(Gtk.Align.START)
        self.container_box.pack_start(self.label, False, False, 6)

        # TODO maybe?: separate this part in separate init_graph method
        self.graph_view = Dazzle.GraphView()
        self.graph_model = Dazzle.GraphModel()
        self.graph_renderer = GraphStackedRenderer()
        self.graph_view.props.height_request = 90
        self.graph_view.props.width_request = 250
        self.graph_renderer.set_line_width(1.5)
        self.graph_color_hex = '#0068F0'
        stroke_color = Gdk.RGBA()
        stroke_color.parse(self.graph_color_hex)
        stacked_color = Gdk.RGBA()
        stacked_color.parse(self.graph_color_hex)
        stacked_color.alpha = 0.5
        self.graph_renderer.set_stroke_color_rgba(stroke_color)
        self.graph_renderer.set_stacked_color_rgba(stacked_color)
        self.graph_model.set_timespan(2000000 * 120) # 2 seconds; time is in microseconds (1 μs = 1 s*10^-6)
        self.graph_model.set_max_samples(120)
        self.graph_model.props.value_max = 100.0
        self.graph_model.props.value_min = 0.0
        
        # apparently can work with multiple data columns, I work with only one
        self.column_data = Dazzle.GraphColumn().new('Data', TYPE_DOUBLE)
        self.graph_model.add_column(self.column_data)

        self.graph_view.set_model(self.graph_model)
        self.graph_view.add_renderer(self.graph_renderer)
        
        self.container_box.pack_start(self.graph_view, False, False, 6)

        self.graph_model_iter = self.graph_model.push(GLib.get_monotonic_time())
        self.graph_model.iter_set(self.graph_model_iter, 0, 0.0)

        self.add(self.container_box)
    
    def on_watcher_new_data(self, *args):
        GLib.idle_add(self.refresh_data_view)

    def refresh_data_view(self):
        current_iter = self.graph_model.push(GLib.get_monotonic_time())
        self.graph_model.iter_set(current_iter, 0, self.watcher.data[-1])
